import random

# Variables
filaInicial = 6
columnaInicial = 6
global matriz1
global navesGeneradas1
global navesDerribadas1
global matriz2
global navesGeneradas2
global navesDerribadas2
global turno
global retiro
global ganador1
global ganador2
global filaIn


class Vehiculo:
    """
    Representa naves, tanques y sus respectivas municiones
    """
    def __init__(self, balas, tipo):
        self.balas = balas
        self.tipo = tipo
        if tipo == " ":
            self.balas = 0


def inicializar_matriz(matriz):
    """
    Inicia la Estructura de la matriz
    """
    navesGeneradas = 0
    lista = ["Tanque", "Nave", " ", " "]
    for i in range(filaInicial):
        matriz.append([0] * columnaInicial)
    for i in range(filaInicial):
        for j in range(columnaInicial):
            tipo = random.choice(lista)
            balas = random.randint(1, 10)
            objecto = Vehiculo(balas, tipo)
            matriz[i][j] = objecto
            if tipo != " ":
                navesGeneradas += 1
    return navesGeneradas


def imprimir_matriz(matriz):
    """
    Imprime la matriz grafica
    :param matriz: Matriz del jugador 1 y Matriz del jugador 2
    :return:Matriz Grafica
    """
    """
    :param matriz: 
    :return: 
    """
    print("\b        0            1             2             3            4             5 \n")
    for i in range(filaInicial):
        linea = f"{i} |"
        for j in range(columnaInicial):
            info_nave = f"{matriz[i][j].tipo}({matriz[i][j].balas}) "
            linea += info_nave.center(12) + "|"
        print(linea, "\n")


# Recorre la matriz para saber la cantidad de municion
def contar_balas(matriz):
    """
     Recorre la matriz para saber la cantidad de municion
    :param matriz: municiones jugador 1 y jugador 2
    :return: Las municiones de cada jugador
    """
    municion = 0
    for i in range(filaInicial):
        for j in range(columnaInicial):
            municion += matriz[i][j].balas
    return municion



def contar_naves_con_armas(matriz):
    """
    Cuenta la cantidad de Vehiculos estan Armados
    """
    navesArmadas = 0
    for i in range(filaInicial):
        for j in range(columnaInicial):
            if int(matriz[i][j].balas) > 0:
                navesArmadas += 1
        return navesArmadas





def disparar(matriz_atacante, matriz_enemiga, fila, columna, navesDerribadas):
    """
    Disparar al rival
    # 1.restar una bala al primer objeto de la matriz que tenga munición
    # 2.verificar si existe un objecto en la matriz enemiga y destruirla (balas 0, tipo vacio)
    """
    restar_municion(matriz_atacante)
    # Determina si le dió o no a una nave enemiga
    if matriz_enemiga[fila][columna].tipo != " ":
        print(f"Has Destruido una Vehiculo {matriz_enemiga[fila][columna].tipo}")
        matriz_enemiga[fila][columna].balas = 0
        matriz_enemiga[fila][columna].tipo = " "
        navesDerribadas += 1
    else:
        print("Has Fallado!!!!")
    return navesDerribadas




def restar_municion(matriz):
    """
    1.Restar una municion al primer objeto de la matriz donde bala sea mayor a 0...

    """

    nave_encontrada = False
    for i in range(filaInicial):
        for j in range(columnaInicial):
            if matriz[i][j].balas > 0:
                matriz[i][j].balas -= 1
                nave_encontrada = True
                break
        if nave_encontrada:
            break


def restarBalas(matriz):
    """
    Resta las municiones Utilizadas de cada jugador
    :param matriz: Matriz de jugador 1 y Matriz de jugador 2
    :return: Municiones
    """
    municion = 0
    for i in range(filaInicial):
        for j in range(columnaInicial):
            municion -= matriz[i][j].balas
    return municion

def Documentacion():
    print("Mi clase :",Vehiculo.__doc__)
    print("Funcion que Inicia la matriz: ",inicializar_matriz.__doc__)
    print("Funcion que Imprime matriz Grafica: ",imprimir_matriz.__doc__)
    print("Funcion que Cuentas las municiones de cada jugador: ", contar_balas.__doc__)
    print("Funcion que Cuenta los Vehiculos armados: ", contar_naves_con_armas.__doc__)
    print("Funcion que Dispara al Terreno enemigo:", disparar.__doc__)
    print("Funcion que Resta las municiones: ", restar_municion.__doc__)
    print("Funcion que Resta las municiones:", restarBalas.__doc__)
    print("Funcion que Determinar ganador: ", determinar_ganador.__doc__)
    print("Funcion que interactua con el usuario: ", jugar.__doc__)



def determinar_ganador(matriz_atacante, matriz_enemiga):
    """
    Determinar ganador

    CONDICIONES
    1. Que uno de los bandos tenga más del 80% más vehículos o naves con armas,
    que el bando contrario
    2. Que uno de los bandos cuente con mas del 70% de municiones que el bando
    contrario.
    """
    navesAtacantes = contar_naves_con_armas(matriz_atacante)
    navesEnemigas = contar_naves_con_armas(matriz_enemiga)
    balasAtacantes = contar_balas(matriz_atacante)
    balasEnemigas = contar_balas(matriz_enemiga)
    porcentajeAtacante = navesAtacantes * 0.2
    # primer condiciones
    if porcentajeAtacante > navesEnemigas:
        return True
    porcentajeBalasAtacante = balasAtacantes * 0.3
    # Segunda Condición
    if porcentajeBalasAtacante > balasEnemigas:
        return True
    return False





def jugar():
    """
    Funcion que interactua con el usuario
    :return: El ganador !!!
    """
    matriz1 = []
    navesGeneradas1 = 0
    navesDerribadas1 = 0
    matriz2 = []
    navesGeneradas2 = 0
    navesDerribadas2 = 0
    turno = True
    retiro = False
    navesGeneradas1 = inicializar_matriz(matriz1)
    navesGeneradas2 = inicializar_matriz(matriz2)
    jugador1 = input("Ingrese el nombre del jugador 1: ")
    jugador2 = input("ingrese el nombre del jugador 2: ")
    ja = jugador1
    ganador1 = False
    ganador2 = False

    while True:
        coordenadarCorrectas = False
        while not coordenadarCorrectas:
            pos = input(f"{ja} ingrese la posicion a Disparar (Fila,Columna) o ingrese (r) para retirarse:  ")
            if pos == "r":
                retiro = True
            else:
                pos = pos.split(",")
                fila = int(pos[0])
                columna = int(pos[1])

                if fila < filaInicial and columna < columnaInicial:
                    coordenadarCorrectas = True
                else:
                    print("Has Ingresado Coordenadas Incorrectas...Vuelve a intentarlo")




        if turno:
            if retiro:
                ganador2 = True
                print(f"-----SE HA RENDIDO--------")
                print(f"{jugador1} Pierde por retirarce")
            else:
                navesDerribadas1 = disparar(matriz1, matriz2, fila, columna, navesDerribadas1)
                ganador1 = determinar_ganador(matriz1, matriz2)
            ja = jugador2
        else:
            if retiro:
                ganador1 = True
                print(f"-----SE HA RENDIDO--------")
                print(f"{jugador2}pierde por retirarse")
            else:
                navesDerribadas2 = disparar(matriz2, matriz1, fila, columna, navesDerribadas2)
                ganador2 = determinar_ganador(matriz2, matriz1)
            ja = jugador1

        if ganador1:
            print(f"-----Datos del Ganador--------\n"
                  f"El ganador es {jugador1}\n"
                  f"Empezó el juego con {navesGeneradas1} naves\n"
                  f"Empezó el juego con {contar_balas(matriz1)} balas\n"
                  f"Derrivó {navesDerribadas1} naves\n"
                  f"le destruyeron {navesDerribadas2} Vehiculos\n\n"
                  f"-----Datos del Perdedor--------\n"
                  f"El perdedor es {jugador2}\n"
                  f"Empezó el juego con {navesGeneradas2} naves\n"
                  f"Empezó el juego con {contar_balas(matriz2)}\n"
                  f"Derrivó {navesDerribadas2} naves\n"
                  f"Le destruyeron {navesDerribadas1} Vehiculos\n\n")
            break

        if ganador2:
            print(f"-----Datos del Ganador--------\n"
                  f"El ganador es {jugador2}\n"
                  f"Empezó el juego con {navesGeneradas2} naves\n"
                  f"Empezó el juego con {contar_balas(matriz2)} balas\n"
                  f"Derrivó {navesDerribadas2} naves\n"
                  f"le destruyeron {navesDerribadas1} Vehiculos\n\n"
                  f"-----Datos del Perdedor--------\n"
                  f"El perdedor es {jugador1}\n"
                  f"Empezó el juego con {navesGeneradas1} naves\n"
                  f"Empezó el juego con {contar_balas(matriz1)}\n"
                  f"Derrivó {navesDerribadas1} Vehiculos\n"
                  f"le destruyeron {navesDerribadas1} Vehiculos\n\n")

            break
        turno = not turno


# ----------------------------------------------------------------------------------------------------------------------

menu = ("1.Iniciar Juego\n"
        "2.Ver Documentacion\n"
        "3.Salir\n"
        "Selecciones su Opción: ")

while True:
    op = int(input(menu))
    if op == 3:
        break
    elif op == 2:
        Documentacion()
    elif op == 1:
        jugar()
        break
